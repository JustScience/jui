import React from 'react';

import Section from '../ui/1-atoms/section';
import Contain from '../ui/1-atoms/contain';
import { HBoom, H1, H2, H3, H4, H5, H6 } from './../ui/1-atoms/headings'
import { P } from '../ui/1-atoms/text';

const Type = () => (
	<React.Fragment>
		<Section Black>
			<Contain Pad>
				<HBoom Center Light>HBoom</HBoom>
			</Contain>
		</Section>
		<Section Secondary>
			<Contain Pad>
				<H1>H1</H1>
				<H2>H2</H2>
				<H3>H3</H3>
				<H4>H4</H4>
				<H5>H5</H5>
				<H6>H6</H6>
				<P>
					<b>P with Left (default) alignment.</b> Freegan small batch sartorial wolf brunch succulents iPhone schlitz try-hard wayfarers stumptown flexitarian kogi hashtag beard. Ethical coloring book migas marfa actually hammock artisan asymmetrical mixtape listicle cloud bread. Pitchfork keffiyeh organic pickled chartreuse af neutra gluten-free green juice next level. Fam master cleanse plaid hoodie chicharrones paleo leggings vaporware. Pabst shaman kogi everyday carry narwhal tumblr.
				</P>
			</Contain>
		</Section>
		<Section Pad>
			<Contain Pad>
				<H2 Left>Left (default)</H2>
				<H2 Center>Center</H2>
				<H2 Right>Right</H2>
				<P Center Faded Breathe>
					<b>P Faded Breathe with Justify alignment.</b> Freegan small batch sartorial wolf brunch succulents iPhone schlitz try-hard wayfarers stumptown flexitarian kogi hashtag beard. Ethical coloring book migas marfa actually hammock artisan asymmetrical mixtape listicle cloud bread. Pitchfork keffiyeh organic pickled chartreuse af neutra gluten-free green juice next level. Fam master cleanse plaid hoodie chicharrones paleo leggings vaporware. Pabst shaman kogi everyday carry narwhal tumblr.
				</P>
			</Contain>
		</Section>
		<Section Primary Pad>
			<Contain Pad>
				<H2 Light Thin>Thin</H2>
				<H2 Light Thick>Thick</H2>
				<H2 Light Thicker>Thicker</H2>
				<P Justify Light Fadeder Squeeze>
					<b>P Fadeder Squeeze with Justify alignment.</b> Freegan small batch sartorial wolf brunch succulents iPhone schlitz try-hard wayfarers stumptown flexitarian kogi hashtag beard. Ethical coloring book migas marfa actually hammock artisan asymmetrical mixtape listicle cloud bread. Pitchfork keffiyeh organic pickled chartreuse af neutra gluten-free green juice next level. Fam master cleanse plaid hoodie chicharrones paleo leggings vaporware. Pabst shaman kogi everyday carry narwhal tumblr.
				</P>
				<P Right Light Spread>
					<b>P Spread with Right alignment.</b> Freegan small batch sartorial wolf brunch succulents iPhone schlitz try-hard wayfarers stumptown flexitarian kogi hashtag beard. Ethical coloring book migas marfa actually hammock artisan asymmetrical mixtape listicle cloud bread. Pitchfork keffiyeh organic pickled chartreuse af neutra gluten-free green juice next level. Fam master cleanse plaid hoodie chicharrones paleo leggings vaporware. Pabst shaman kogi everyday carry narwhal tumblr.
				</P>
			</Contain>
		</Section>
	</React.Fragment>
);

export default Type;
