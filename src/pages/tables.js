import React from 'react';

import Section from '../ui/1-atoms/section';
import Contain from '../ui/1-atoms/contain';
import { HBoom, H1 } from './../ui/1-atoms/headings'
import { Table, TBody, THead, TFoot, TR, TH, TD, TableIMG } from '../ui/1-atoms/tables';
import { P, B, EM } from '../ui/1-atoms/text';

const Tables = () => (
	<React.Fragment>
		<Section Black>
			<Contain Pad>
				<HBoom Center Light CAPS>Tables</HBoom>
			</Contain>
		</Section>
		<Section Secondary>
			<Contain Pad>
				<H1>Default Table</H1>
				<Contain Pad>
					<Table>
						<THead>
							<TR>
								<TH Left>TH Left</TH>
								<TH>TH (default)</TH>
								<TH Right>TH Right</TH>
							</TR>
						</THead>
						<TBody>
							<TR>
								<TD Left>TD Left</TD>
								<TD>TD (default)</TD>
								<TD Right>TD Right</TD>
							</TR>
							<TR>
								<TD Left>TD Left</TD>
								<TD>TD (default)</TD>
								<TD Right>TD Right</TD>
							</TR>
							<TR>
								<TD Left>TD Left</TD>
								<TD>TD (default)</TD>
								<TD Right>TD Right</TD>
							</TR>
							<TR>
								<TD Left>TD Left</TD>
								<TD><TableIMG src='http://www.fillmurray.com/96/54' /></TD>
								<TD Right>TD Right</TD>
							</TR>
						</TBody>
						<TFoot>
							<TR>
								<TD Left>Footer TD Left</TD>
								<TD>Footer TD (default)</TD>
								<TD Right>Footer TD Right</TD>
							</TR>
						</TFoot>
					</Table>
				</Contain>
				<P>
					<B>Table with Center (default), Left, and Right aligned cell text.</B> Freegan small batch sartorial wolf brunch succulents iPhone schlitz try-hard wayfarers stumptown flexitarian kogi <EM>hashtag beard</EM>. Ethical coloring book migas marfa actually hammock artisan asymmetrical mixtape listicle cloud bread. Pitchfork keffiyeh organic pickled chartreuse af neutra gluten-free green juice next level. Fam master cleanse plaid hoodie chicharrones paleo leggings vaporware. Pabst shaman kogi everyday carry narwhal tumblr.
				</P>
			</Contain>
		</Section>
	</React.Fragment>
);

export default Tables;
