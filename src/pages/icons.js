import React from 'react'

import Section from '../ui/1-atoms/section'
import Contain from '../ui/1-atoms/contain'
import { HBoom } from './../ui/1-atoms/headings'
import { P } from '../ui/1-atoms/text'
import Icon from '../ui/1-atoms/icons/icon'
import FlexRow from '../ui/2-molecules/flex-row'
import { Spacer } from '../ui/1-atoms/layout'

import color from '../ui/0-particles/color'


const Icons = () => (
	<React.Fragment>
		<Section Black>
			<Contain Pad>
				<HBoom Center Light>Icons</HBoom>
			</Contain>
		</Section>
		<Section Secondary>
			<Contain Pad>

				<FlexRow>
					<Icon shape='bill' />
					<Icon shape='cart' />
					<Icon shape='check' />
					<Icon shape='memo' />
					<Icon shape='pig' />
					<Icon shape='star' />
					<Icon shape='starOutline' />
					<Icon shape='burger' />
					<Icon shape='close' />
				</FlexRow>
				<Spacer />
				<FlexRow>
					<Icon shape='bill' color={color.primary.dark} />
					<Icon shape='cart' color={color.accent} />
					<Icon shape='check' color={color.primary.light} />
					<Icon shape='memo' color='aqua' />
					<Icon shape='pig' color='yellow' />
					<Icon shape='star' color='pink' />
					<Icon shape='starOutline' color='red' />
					<Icon shape='burger' color='green' />
					<Icon shape='close' color='grey' />
				</FlexRow>


				<P>
					<b>A <strong>Single Icon</strong> that takes many shapes and colors, thanks to the magic of <strong>SVG</strong>.</b> Freegan small batch sartorial wolf brunch succulents iPhone schlitz try-hard wayfarers stumptown flexitarian kogi hashtag beard. Ethical coloring book migas marfa actually hammock artisan asymmetrical mixtape listicle cloud bread. Pitchfork keffiyeh organic pickled chartreuse af neutra gluten-free green juice next level. Fam master cleanse plaid hoodie chicharrones paleo leggings vaporware. Pabst shaman kogi everyday carry narwhal tumblr.
				</P>

			</Contain>
		</Section>
	</React.Fragment>
)

export default Icons;