import React from 'react';

import Section from '../ui/1-atoms/section';
import Contain from '../ui/1-atoms/contain';
import { HBoom } from './../ui/1-atoms/headings'
import Form from '../ui/1-atoms/inputs/form';
import Input from '../ui/1-atoms/inputs/input';
import RadioGroup from '../ui/1-atoms/inputs/radio-group'

const Forms = () => (
	<React.Fragment>
		<Section Black>
			<Contain Pad>
				<HBoom Center Light CAPS>Forms</HBoom>
			</Contain>
		</Section>
		<Section Accent>
			<Contain Pad>
				<Form title={'Form Title'}>
					<RadioGroup name='radios' />
					<br />
					<Input type={'text'} htmlFor={'demo1'} labelText={'labelText'} placeholder={'width="full" (default)'} width={'full'} />
					<Input type={'text'} htmlFor={'demo2'} labelText={'labelText'} placeholder={'width="half"'} width={'half'} />
					<Input type={'text'} htmlFor={'demo3'} labelText={'labelText'} placeholder={'width="quarter"'} width={'quarter'} />
					<Input type={'text'} htmlFor={'demo4'} labelText={'labelText'} placeholder={'width="quarter"'} width={'quarter'} />
				</Form>
			</Contain>
		</Section>
		<Section>
			<Contain Pad>
				<Form Primary title={'Primary Form'}>
					<Input type={'text'} htmlFor={'demo5'} labelText={'labelText'} placeholder={'width="full" (default)'} width={'full'} />
					<Input type={'text'} htmlFor={'demo6'} labelText={'labelText'} placeholder={'width="half"'} width={'half'} />
					<Input type={'text'} htmlFor={'demo7'} labelText={'labelText'} placeholder={'width="quarter"'} width={'quarter'} />
					<Input type={'text'} htmlFor={'demo8'} labelText={'labelText'} placeholder={'width="quarter"'} width={'quarter'} />
				</Form>
			</Contain>
		</Section>
		<Section Primary>
			<Contain Pad>
				<Form Dark title={'Dark Form'}>
					<Input type={'text'} htmlFor={'demo9'} labelText={'labelText'} placeholder={'width="full" (default)'} width={'full'} />
					<Input type={'text'} htmlFor={'demo10'} labelText={'labelText'} placeholder={'width="half"'} width={'half'} />
					<Input type={'text'} htmlFor={'demo11'} labelText={'labelText'} placeholder={'width="quarter"'} width={'quarter'} />
					<Input type={'text'} htmlFor={'demo12'} labelText={'labelText'} placeholder={'width="quarter"'} width={'quarter'} />
				</Form>
			</Contain>
		</Section>
	</React.Fragment>
);

export default Forms;
