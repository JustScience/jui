import React from 'react';

import Section from '../ui/1-atoms/section';
import Contain from '../ui/1-atoms/contain';
import { HBoom, H1, H2 } from './../ui/1-atoms/headings'
import { P } from '../ui/1-atoms/text';

const Home = () => (
	<React.Fragment>
		<Section Black>
	  	<Contain Pad>
		    <HBoom Center Light>Hello World</HBoom>
	    </Contain>
		</Section>
		<Section Secondary>
	  	<Contain Pad>
		    <H1 Center>JSci UI</H1>
		    <P Center Big Thin>
		    	Built for Sassy Designers in a Reactive World.
	    	</P>
		    <P Center Bigger Light Bolder>
		    	Atomic Design + Styled Components
	    	</P>
	    </Contain>
		</Section>
		<Section Pad>
	  	<Contain Pad>
		    <H2 Center>Why?</H2>
		    <P Faded Big Thin>
		    	Because!
	    	</P>
		    <P Justify Center Big>
		    	I just thought we needed yet another styled ui component library for yet another javascript framework. 
	    	</P>
		    <P Fadeder Right Thin Bigger>
		    	Mine's just better, Ob-V.
	    	</P>
	    </Contain>
		</Section>
	</React.Fragment>
);

export default Home;
