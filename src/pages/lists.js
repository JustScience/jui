import React from 'react';

import Section from '../ui/1-atoms/section';
import Center from '../ui/1-atoms/center';
import Contain from '../ui/1-atoms/contain';
import { HBoom, H2, H3 } from './../ui/1-atoms/headings'
import { UL, LI, GL, GI } from '../ui/2-molecules/lists';
import IMG from '../ui/1-atoms/images';
import Card from '../ui/3-organisms/card';

const Lists = () => (
	<React.Fragment>
		<Section Black>
			<Contain Pad>
				<HBoom Center Light CAPS>Lists</HBoom>
			</Contain>
		</Section>
		<Section Accent Pad>
			<Contain Narrow>
				<H2>UL</H2>
				<br />
				<H3 Light Rise>Unordered List (default)</H3>
				<UL>
					<LI>LI</LI>
					<LI>LI</LI>
					<LI>LI</LI>
				</UL>
				<br />
				<H3 Light RiseUp>Unordered List (Inline)</H3>
				<UL Inline>
					<LI>LI</LI>
					<LI>LI</LI>
					<LI>LI</LI>
				</UL>
			</Contain>
		</Section>
		<Section Pad>
			<Center>
				<Contain Narrow>
					<H3 Center Rise>Image List Inline</H3>
					<UL Inline>
						<LI>
							<IMG src={'http://www.fillmurray.com/150/120'}/>
						</LI>
						<LI>
							<IMG src={'http://www.fillmurray.com/150/120'}/>
						</LI>
						<LI>
							<IMG src={'http://www.fillmurray.com/150/120'}/>
						</LI>
					</UL>
				</Contain>
			</Center>
		</Section>
		<Section Primary Pad>
			<Center>
				<Contain Narrow Pad>
					<H3 Light Center RiseUp>Card List</H3>
					<UL>
						<LI>
							<Card title={'Card Title'} />
						</LI>
						<LI>
							<Card title={'Card Title'} />
						</LI>
						<LI>
							<Card title={'Card Title'} />
						</LI>
					</UL>
				</Contain>
			</Center>
		</Section>
		<Section Pad>
			<Center>
				<Contain Pad>
					<H3 Center RiseUp>Grid Gallery</H3>
					<GL>
						<GI>
							<Card title={'Card Title'} src={'http://www.fillmurray.com/90/90'} imgHeight={'90px'}/>
						</GI>
						<GI>
							<Card title={'Card Title'} src={'http://www.fillmurray.com/90/90'} imgHeight={'90px'}/>
						</GI>
						<GI>
							<Card title={'Card Title'} src={'http://www.fillmurray.com/90/90'} imgHeight={'90px'}/>
						</GI>
						<GI>
							<Card title={'Card Title'} src={'http://www.fillmurray.com/90/90'} imgHeight={'90px'}/>
						</GI>
						<GI>
							<Card title={'Card Title'} src={'http://www.fillmurray.com/90/90'} imgHeight={'90px'}/>
						</GI>
						<GI>
							<Card title={'Card Title'} src={'http://www.fillmurray.com/90/90'} imgHeight={'90px'}/>
						</GI>
						<GI>
							<Card title={'Card Title'} src={'http://www.fillmurray.com/90/90'} imgHeight={'90px'}/>
						</GI>
						<GI>
							<Card title={'Card Title'} src={'http://www.fillmurray.com/90/90'} imgHeight={'90px'}/>
						</GI>
						<GI>
							<Card title={'Card Title'} src={'http://www.fillmurray.com/90/90'} imgHeight={'90px'}/>
						</GI>
						<GI>
							<Card title={'Card Title'} src={'http://www.fillmurray.com/90/90'} imgHeight={'90px'}/>
						</GI>
						<GI>
							<Card title={'Card Title'} src={'http://www.fillmurray.com/90/90'} imgHeight={'90px'}/>
						</GI>
						<GI>
							<Card title={'Card Title'} src={'http://www.fillmurray.com/90/90'} imgHeight={'90px'}/>
						</GI>
						<GI>
							<Card title={'Card Title'} src={'http://www.fillmurray.com/90/90'} imgHeight={'90px'}/>
						</GI>
						<GI>
							<Card title={'Card Title'} src={'http://www.fillmurray.com/90/90'} imgHeight={'90px'}/>
						</GI>
						<GI>
							<Card title={'Card Title'} src={'http://www.fillmurray.com/90/90'} imgHeight={'90px'}/>
						</GI>
						<GI>
							<Card title={'Card Title'} src={'http://www.fillmurray.com/90/90'} imgHeight={'90px'}/>
						</GI>
						<GI>
							<Card title={'Card Title'} src={'http://www.fillmurray.com/90/90'} imgHeight={'90px'}/>
						</GI>
					</GL>
				</Contain>
			</Center>
		</Section>
	</React.Fragment>
);

export default Lists;
