import React from 'react';

import Section from '../ui/1-atoms/section';
import Contain from '../ui/1-atoms/contain';
import { HBoom, H3 } from './../ui/1-atoms/headings'
import { Spacer, Gutter } from './../ui/1-atoms/layout'
import BTN from './../ui/1-atoms/buttons'
import Toggle from './../ui/1-atoms/inputs/toggle'
import Counter from './../ui/1-atoms/inputs/counter'

const Buttons = () => (
	<React.Fragment>
		<Section Black>
			<Contain Pad>
				<HBoom Center Light CAPS>Buttons</HBoom>
			</Contain>
		</Section>
		<Section Accent>
			<Contain Pad>

				<H3>Flat Buttons</H3>

				<BTN 
					Inline Space 
					label='Default' 
				/>

				<BTN 
					Light Inline Space
					label='Light' 
				/>

				<BTN 
					Dark Inline Space 
					label='Dark' 
				/>

				<BTN 
					Primary Inline Space
					label='Primary' 
				/>

				<BTN 
					Secondary Inline Space
					label='Secondary' 
				/>

				<Spacer />

				<H3>Raised Buttons</H3>

				<BTN 
					Raised Inline Space 
					label='Default' 
				/>

				<BTN 
					Raised Light Inline Space
					label='Light' 
				/>

				<BTN 
					Raised Dark Inline Space 
					label='Dark' 
				/>

				<BTN 
					Raised Primary Inline Space
					label='Primary' 
				/>

				<BTN 
					Raised Secondary Inline Space
					label='Secondary' 
				/>

				<Spacer />

				<H3>Toggle Switch</H3>
				<Spacer Small />
				<Toggle	/>		
				<Gutter Small />
				<Toggle			
					onLabel='On'
					offLabel='Off'
					width='60px' 
				/>
				<Spacer />

				<H3>Counter</H3>
				<Spacer Small />
				<Counter />		
				<Spacer />

			</Contain>
		</Section>
	</React.Fragment>
);

export default Buttons;
