import React from 'react'
import styled from 'styled-components'


const FormGroup = styled.div.attrs({
	className: 'Radio_Group'
})`
	display: flex;
	flex-wrap: wrap;
	justify-content: space-around;
`

const RadioWrap = styled.label.attrs({
	className: 'Radio_Button'
})`
	align-items: center;
	background: #FFFFFF;
	border: ${props => 
		props.selected ? '2px solid LimeGreen' 
		: '2px solid #FFFFFF'};
	border-radius: 2px;
	box-sizing: border-box;
	cursor: pointer;
	display: flex;
	flex-grow: 1;
	height: ${props => 
		props.height ? props.height 
		: '36px'};
	justify-content: space-around;
	margin: 4px;
	position: relative;
	text-align: center;
	width: ${props => 
		props.width ? props.width 
		: '90px'};
`

const RadioInput = styled.input.attrs({
	type: 'radio',
	name: props => props.name ? props.name : 'radioGroup',
	className: 'Radio_Input'
})`
	position: absolute;
	top: 0;
	left: 0;
	opacity: 0;
`

const RadioLabel = styled.p.attrs({
	className: 'Radio_Label'
})`
	color: #666666;
	font-size: 12px;
	margin: 0;
	padding: 0;
	padding-left: ${props => 
			props.selected ? '12px' 
			: '0'};
	position: relative;
	transition: all 333ms ease-out;
  -webkit-touch-callout: none;
    -webkit-user-select: none;
     -khtml-user-select: none;
       -moz-user-select: none;
        -ms-user-select: none;
            user-select: none;

	&:before {
		color: ${props => 
			props.selected ? 'LimeGreen' 
			: 'transparent'};
		content: '\u2713';
		font-size: 18px;
		line-height: 78%;
		left: ${props => 
			props.selected ? '-6px' 
			: '3px'};
		position: absolute;
		transition: all 333ms ease-out;
	}
`


// COMPONENT TEMPLATING HERE
const Radio = (props) => {

	return (
		<RadioWrap selected={props.selected}>
			<RadioInput onClick={props.onRadioSelected} name={props.name} />
			<RadioLabel selected={props.selected}>
				{props.radioLabel}
			</RadioLabel>
		</RadioWrap>
	)

}

const values = ['Apple', 'Orange', 'Banana', 'Grapefruit', 'Plum', 'Peach']

const RadioGroupSection = (props) => {
	const name = props.name
	
	const radios = values.map((value,index) => {
		return (
			<Radio key={index} radioLabel={value} name={name} onRadioSelected={()=>{props.onRadioSelected(index)}} selected={props.selectedRadioKey===index} ></Radio>
		)
	})

	return (
		<React.Fragment>
			{radios}
		</React.Fragment>	
	)
	
}


// FULL COMPONENT
class RadioGroup extends React.Component {
	constructor(props) {
		super(props);
		this.state = ({selectedRadio: ''})
		this.radioSelected = this.radioSelected.bind(this);
	}

	render() {
		return (
			<FormGroup>
				<RadioGroupSection selectedRadioKey={this.state.selectedRadioKey} onRadioSelected={this.radioSelected} name={this.props.name} />
			</FormGroup>
		)
	}

	radioSelected(radioKey){
		this.setState({selectedRadioKey: radioKey});
	}
	
}

export default RadioGroup;