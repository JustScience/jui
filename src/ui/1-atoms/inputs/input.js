import React, { Component } from "react";
import styled from "styled-components";

import form from "./../../0-particles/form";
import layout from "./../../0-particles/layout";

import InputLabel from "./label";

const InputField = styled.input.attrs({
  type: (props) => props.type || "text",
  value: (props) => props.value,
  htmlFor: (props) => props.htmlFor,
  name: (props) => props.htmlFor,
  id: (props) => props.htmlFor,
  placeholder: (props) => props.placeholder || "",
  maxLength: (props) => props.maxLength || "",
  onChange: (props) => props.onChange,
  icon: (props) => props.icon,
})`
  background-color: ${form.input.BG};
  border: ${form.input.border};
  border-bottom: ${form.input.borderBottom};
  border-radius: ${layout.inputRadius};
  border-color: ${form.input.borderColor};
  box-shadow: ${form.input.shadow};
  box-sizing: border-box;
  color: ${form.input.color};
  font-size: 120%;
  font-weight: 500;
  margin: 3px 0;
  margin-bottom: 9px;
  outline: none;
  padding: 9px 15px 10px 21px;
  padding-left: ${(props) => (props.icon ? "54px" : "15px")};
  position: relative;
  transition: all 333ms ease-out;
  width: 100%;

  &:focus {
    border-bottom: ${form.input.focusBorderBottom};
    border-color: ${form.input.focusBorderColor};
    box-shadow: ${form.input.focusShadow};
    border: 1px solid ${form.input.focusBorderColor};
    background-color: ${form.input.focusBG};
  }

  &.input {
  }

  &.--has-error {
    color: #ca1061;
    border: apx solid #ca1061;
  }

  ::-webkit-input-placeholder {
    color: ${form.input.placeholder};
    padding-top: 1px;
  }
  :-ms-input-placeholder {
    color: ${form.input.placeholder};
    padding-top: 1px;
  }
  ::-moz-placeholder {
    color: ${form.input.placeholder};
    padding-top: 1px;
  }
  :-moz-placeholder {
    color: ${form.input.placeholder};
    padding-top: 1px;
  }

  ${props => props.styles};
`;

const InputIcon = styled.i.attrs({
  icon: (props) => props.icon,
})`
  background-color: orange;
  border-radius: 30px 0 0 30px;
  color: #ffffff;
  font-size: 150%;
  left: 0;
  line-height: 50%;
  padding: 11px 9px 12px 12px;
  position: absolute;
  top: 25px;

  ${props => props.styles};
`;

export default class Input extends Component {
  render() {
    return (
      <InputLabel labelText={this.props.labelText} htmlFor={this.props.htmlFor} width={this.props.width}>
        <InputField
          type={this.props.type}
          className={"input--" + this.props.type + (this.props.hasError ? " --has-error" : "")}
          id={this.props.htmlFor}
          name={this.props.htmlFor}
          placeholder={this.props.placeholder}
          value={this.props.value}
          maxLength={this.props.maxLength}
          onChange={this.props.onChange}
          icon={this.props.icon}
          readOnly={this.props.readOnly}
        />
        {this.props.icon && <InputIcon className="input-field_icon material-icons">{this.props.icon}</InputIcon>}
      </InputLabel>
    );
  }
}
