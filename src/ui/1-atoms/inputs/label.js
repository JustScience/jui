import React from "react";
import styled from "styled-components";

import color from "./../../0-particles/color";
import form from "./../../0-particles/form";
import layout from "./../../0-particles/layout";
import media from "./../../0-particles/media";


const Label = styled.label`
  color: ${form.label};
  width: 99%;
  margin-left: 0.5%;
  margin-right: 0.5%;

  &.full {
    width: 99%;
    margin-left: 0.5%;
    margin-right: 0.5%;
  }

  &.half {
    float: left;
    width: 100%;

    ${media.tabletSM`
      width: 49.5%;
      margin-left: 0.25%;
      margin-right: 0.25%;

      &.last {
        margin-right: 0;
      }

      &.padded {
        padding-right: 12px;
      }
    `}
  }

  &.quarter {
    float: left;
    width: 49.5%;
    margin-left: 0.25%;
    margin-right: 0.25%;

    ${media.tabletSM`
      width: 24.5%;
      margin-left: 0.25%;
      margin-right: 0.25%;
    `}

    &.last {
      margin-right: 0;
    }

    &.padded {
      padding-right: 12px;
    }
  }

  ${props => props.styles};
`;


const LabelText = styled.span`
  color: ${props => props.Light ? color.white : form.label};
  font-size: 75%;
  font-weight: 500;
  margin-left: calc(${layout.marginBase}*5);

  ${props => props.styles};
`;

const InputLabel = (props) => {
  const width = props.width || "full";
  const htmlFor = props.htmlFor || "";
  const labelText = props.labelText || "Field Label";

  return (
    <Label className={"input-label " + width} htmlFor={htmlFor}>
      <LabelText className="input-label_text">{labelText}</LabelText>
      {props.children}
    </Label>
  );
};

export default InputLabel;
