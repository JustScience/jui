import React, {Component} from 'react';
import styled from 'styled-components';

import color from './../../../ui/0-particles/color';

const Block = styled.div`
	align-items: center;
	background: ${color.offWhite};
	box-sizing: border-box;
	display: flex;
	height: auto;
	justify-content: center;
	padding: 12px;
`

const Value = styled.p`
	color: ${color.primary.base};
	font-size: 3em;
	font-weight: 900;
	line-height: 100%;
	margin: 0 24px;
`

const Button = styled.div`
	align-items: center;
	background: ${color.primary.base};
	border-radius: 9px;
	cursor: pointer;
	display: flex;
	height: 30px;
	justify-content: center;
	width: 30px;

	p {
		color:  ${color.white};	
		font-size: 2em;
		font-weight: 900;
		line-height: 0;
		margin: 0;
		padding: 0;
	}
`



// COMPONENT TEMPLATING HERE
class Counter extends Component {

	constructor (props) {
			super(props)
			this.increment = this.increment.bind(this)
			this.decrement = this.decrement.bind(this)
			this.state = {
					count: 0
			}
	}

	increment (event) {
			this.setState({ count: this.state.count + 1 })
			console.log('Count is updated to ' + (this.state.count + 1))
	}

	decrement (event) {
			this.setState({ count: this.state.count - 1 })
			console.log('Count is updated to ' + (this.state.count - 1))
	}


	render (props) {
		return (
			
			<React.Fragment>
				<Block>
					<Button onClick={this.decrement}><p>-</p></Button>				
					<Value>{this.state.count}</Value>
					<Button onClick={this.increment}><p>+</p></Button>
				</Block>
			</React.Fragment>
			
		)
	}
};

export default Counter;
