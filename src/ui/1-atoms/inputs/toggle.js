import React, {Component} from 'react';
import styled from 'styled-components';

const Switch = styled.label`
	border-radius: 18px;
	cursor: pointer;
	display: inline-block;
	height: 24px;
	position: relative;
	vertical-align: top;
	width: ${props => props.width};

	${props => props.styles};
`;

const Checkbox = styled.input`
	position: absolute;
	top: 0;
	left: 0;
	opacity: 0;

	${props => props.styles};
`;

const Slider = styled.span`
	position: relative;
	display: block;
	height: inherit;
	font-size: 14px;
	text-transform: capitalize;
	background: ${props => props.checked ? 'LimeGreen' : 'rgba(0,0,0,0.15)'};
	border-radius: inherit;
	box-shadow: ${props => props.checked ? 'inset 0 0 3px rgba(0,0,0,0.6)' : 'inset 0 0 3px rgba(0,0,0,0.36)'};
	transition: 0.15s ease-out;
	transition-property: opacity background;

	&:before, &:after {
		position: absolute;
		top: 50%;
		margin-top: -.5em;
		line-height: 1;
		transition: inherit;  
	}

	&:before {
		color: white;
		content: ${props => props.onLabel ? '"'+props.offLabel+'"' : ''};
		right: 0;
		padding: 0 12px;
		opacity: ${props => props.checked ? '0' : '1'};
	}

	&:after {
		color: white;
		content: ${props => props.onLabel ? '"'+props.onLabel+'"' : ''};
		left: 0;
		padding: 0 12px;
		opacity: ${props => props.checked ? '1' : '0'};
	}

	${props => props.styles};
`;

const Handle = styled.div.attrs({
	width: props => props.width
})`
	position: absolute;
	top: 2px;
	left: ${props => props.checked ? 'calc(' + props.width + ' - 22px)' : '2px'};
	width: 20px;
	height: 20px;
	background: white;
	border-radius: 50%;
	box-shadow: 0 3px 6px -3px rgba(0,0,0,0.9);
	transition: left 0.15s ease-out;

	${props => props.styles};
`;


// COMPONENT TEMPLATING HERE
class Toggle extends Component {

	constructor (props) {
			super(props)
			this.handleClick = this.handleClick.bind(this)
			this.state = {
					checked: !!(props.checked || props.defaultChecked),
			}
	}

	handleClick (event) {
			this.setState({checked: !this.state.checked })
			console.log('toggle clicked and set to ' + this.state.checked)
	}

	render (props) {
		return (
			<React.Fragment>
				<Switch className='toggle_block' width={this.props.width || '60px'} >
					<Checkbox
						type='checkbox'
						className='toggle_input'
						checked={this.state.checked}
						onClick={this.handleClick}
					/>
					<Slider 
						className='toggle_slider' 
						checked={this.state.checked}						
						onLabel={this.props.onLabel}
						offLabel={this.props.offLabel}
						width={this.props.width || '60px'} 
					/>
					<Handle 
						className='toggle_handle' 
						checked={this.state.checked} 
						width={this.props.width || '60px'} 
					/>
				</Switch>
			</React.Fragment>
		)
	}
};
export default Toggle;