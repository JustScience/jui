import React from 'react';
import styled from 'styled-components';

import color from './../../0-particles/color';
import font from './../../0-particles/font';
import form from './../../0-particles/form';

const FormWrap = styled.form`
	color: ${props => 
		props.Dark ? font.light :
		props.Primary ? color.primary.base 
		: 'none'};
	margin-left: auto;
	margin-right: auto;
	padding: 18px;
	max-width: 900px;
	
	input {
		border-bottom: ${props => 
			props.Dark ? '1px solid' + font.light 
			: form.input.borderBottom};
		
		::-webkit-input-placeholder {
			color: ${props =>
				props.Dark ? form.input.placeholderDark :
				props.Primary ? form.input.placeholderPrimary
				: form.input.placeholder};
		}
		:-ms-input-placeholder {
			color: ${props =>
				props.Dark ? form.input.placeholderDark :
				props.Primary ? form.input.placeholderPrimary
				: form.input.placeholder};
		}
		::-moz-placeholder {
			color: ${props =>
				props.Dark ? form.input.placeholderDark :
				props.Primary ? form.input.placeholderPrimary
				: form.input.placeholder};
		}
		:-moz-placeholder {
			color: ${props =>
				props.Dark ? form.input.placeholderDark :
				props.Primary ? form.input.placeholderPrimary
				: form.input.placeholder};
		}
		&:focus {
			::-webkit-input-placeholder {
				color: ${props =>
					props.Primary ? form.input.placeholderPrimary
					: form.input.placeholder};
			}
			:-ms-input-placeholder {
				color: ${props =>
					props.Primary ? form.input.placeholderPrimary
					: form.input.placeholder};
			}
			::-moz-placeholder {
				color: ${props =>
					props.Primary ? form.input.placeholderPrimary
					: form.input.placeholder};
			}
			:-moz-placeholder {
				color: ${props =>
					props.Primary ? form.input.placeholderPrimary
					: form.input.placeholder};
			}
		}
	}

	&:before,
	&:after {
		content: " ";
		display: table;
	}
	&:after {
		clear: both;
	}

	// ALLOW FOR CUSTOM STYLE OVERRIDES
	${props => props.styles};
`;

const FormTitle = styled.h4`
	font-size: ${font.big};
	font-weight: ${font.thick};
	margin: 0;
	margin-bottom: 9px;
	padding: 0;
	width: 100%;

	${props => props.styles};
`;

const Form = (props) => {
	let color = props.color || '';
	let title = props.title || '';
 
	return (
		<FormWrap {...props} width={props.width} color={color}>
			<FormTitle>{title}</FormTitle>
			{props.children}
		</FormWrap>
	)
};

export default Form;