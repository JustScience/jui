import React from "react";
import styled from "styled-components";

import color from "./../../0-particles/color";

const FormCheck = styled.label`
  color: ${color.dkGrey};
  font-weight: 500;
  padding-left: 9px;

  // ALLOW FOR CUSTOM STYLE OVERRIDES
  ${props => props.styles};
`;

const CheckBox = styled.input`
  width: auto;
  display: inline-block;

  ${props => props.styles};
`;

const CheckLabel = styled.span`
  cursor: pointer;
  width: auto;
  display: inline-block;

  ${props => props.styles};
`;

const Checkbox = (props) => {
  return (
    <FormCheck className={"form-check "} htmlFor={props.htmlFor}>
      <CheckBox type="checkbox" className="form-check-input input--checkbox" checked={props.checked} onClick={(event) => props.onClick(!props.checked)} />
      <CheckLabel className="radio-input-label">{props.labelText}</CheckLabel>
    </FormCheck>
  );
};

export default Checkbox;
