import styled from 'styled-components';

import color from './../0-particles/color';

export const HR = styled.hr`
	border-color: ${color.primary.base}

	${props => props.styles};
`

export const BR = styled.br`

	${props => props.styles};
`

export const Spacer = styled.div`
	height: ${props =>
		props.Small ? '12px' :
		props.Big ? '36px' :
		props.Bigger ? '72px'
		: '24px'
	};
`
export const Gutter = styled.div`
	display: inline-block;
	width: ${props =>
		props.Small ? '12px' :
		props.Big ? '36px' :
		props.Bigger ? '72px'
		: '24px'
	};
`
