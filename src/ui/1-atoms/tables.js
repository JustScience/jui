import styled, {css} from "styled-components";

import color from './../0-particles/color';
import font from './../0-particles/font';
import layout from './../0-particles/layout';

const tableBase = css`
	border: none;
	margin: 0;
	padding: 0;
	width: 100%;
`;

const cellBase = css`
	border-bottom: 1px solid rgba(0,0,0,0.06);
	border-right: 1px solid rgba(0,0,0,0.06);
	color: ${font.color};
	text-align: ${props =>
		props.Left ? 'left' :
		props.Right ? 'right'
		: 'center'};
`;

export const Table = styled.table`
	${tableBase};
	background: ${color.white};
	margin-top: calc(${layout.space.margin}*5);

	${props => props.styles};
`;

export const THead = styled.thead`
	${tableBase};

	${props => props.styles};
`;

export const TBody = styled.tbody`
	${tableBase};

	${props => props.styles};
`;

export const TFoot = styled.tfoot`
	${tableBase};
	td {
		font-weight: ${font.thick};
		padding: calc(${layout.space.padding}*3) calc(${layout.space.padding}*5);
	}

	${props => props.styles};
`;

export const TR = styled.tr`
	${tableBase};
	border-bottom: 1px solid rgba(0,0,0,0.15);
	th:last-of-type {border-right: none;}
	td:last-of-type {border-right: none;}

	${props => props.styles};
`;

export const TH = styled.th`
	${cellBase};
	color: ${color.ltGrey};
	font-size: ${font.small};
	font-weight: ${font.thick};
	padding: calc(${layout.space.padding}*3) calc(${layout.space.padding}*5);

	${props => props.styles};
`;

export const TD = styled.td`
	${cellBase};
	padding: calc(${layout.space.padding}*5) calc(${layout.space.padding}*6);

	${props => props.styles};
`;

export const TableIMG = styled.img.attrs({
	src: props => props.src ? props.src : 'http://www.fillmurray.com/90/60',
})`
  	height: ${props =>
  		props.Small ? '30px'
  		: undefined};
  	width: ${props =>
  		props.Small ? '30px'
  		: undefined};
	object-fit: contain;
`;

