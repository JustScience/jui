import styled, { css } from 'styled-components';

import color from "./../0-particles/color";
import layout from "./../0-particles/layout";
import font from "./../0-particles/font";

const baseStyles = css`
  @import url('https://fonts.googleapis.com/css?family=Montserrat:300,500,700,800,900');
	color: ${props => 
    props.Fadeder ? font.fadeder : 
    props.Faded ? font.faded : 
    props.Light ? font.light : 
    props.Dark ? font.dark : 
    props.Primary ? color.primary.base : 
    props.Secondary ? color.secondary.base : 
    font.color};
  font-family: 'Montserrat', helvetica, sans-serif;
  line-height: 1.2;
  margin: 0;
  margin-bottom: ${props => 
    props.Rise ? 'calc(' + layout.space.margin + '*3)' :
    props.RiseUp ? 'calc(' + layout.space.margin + '*6)' :
    '0'};
  padding: 0;
  text-transform: ${props => 
    props.Lower ? 'lowecase' : 
    props.Caps ? 'capitalize' : 
    props.CAPS ? 'uppercase' : 
    'none'};
`;

const heading = styled.span`
  font-weight: ${props =>
    props.Thin ? font.thin :
    props.Thick ? font.thick :
    props.Thicker ? font.thicker :
    '900'
  };
  text-align: ${props => 
    props.Left ? 'left' : 
    props.Center ? 'center' : 
    props.Right ? 'right' : 
    props.Justify ? 'justify' : 
    'left'
  };
`;

export const HBoom = heading.withComponent('h1').extend`
  ${baseStyles}
  font-size: 4.5rem;

  ${props => props.styles};
`;

export const H1 = heading.withComponent('h1').extend`
  ${baseStyles}
  font-size: 3rem;
  line-height: 1.0;

  ${props => props.styles};
`;

export const H2 = heading.withComponent('h2').extend`
  ${baseStyles}
  font-size: 2.1rem;
  font-weight: ${props =>
    props.Thin ? font.thin :
    props.Thick ? font.thick :
    props.Thicker ? font.thicker :
    '700'
  };

  ${props => props.styles};
`;

export const H3 = heading.withComponent('h3').extend`
  ${baseStyles}
  font-size: 1.5rem;
  font-weight: ${props =>
    props.Thin ? font.thin :
    props.Thick ? font.thick :
    props.Thicker ? font.thicker :
    '300'
  };

  ${props => props.styles};
`;

export const H4 = heading.withComponent('h4').extend`
  ${baseStyles}
  font-size: 1.2rem;

  ${props => props.styles};
`;

export const H5 = heading.withComponent('h5').extend`
  ${baseStyles}
  font-size: 0.9rem;
  font-weight: ${props =>
    props.Thin ? font.thin :
    props.Thick ? font.thick :
    props.Thicker ? font.thicker :
    '700'
  };

  ${props => props.styles};
`;

export const H6 = heading.withComponent('h6').extend`
  ${baseStyles}
  font-size: 0.75rem;
  font-weight: ${props =>
    props.Thin ? font.thin :
    props.Thick ? font.thick :
    props.Thicker ? font.thicker :
    '300'
  };
  line-height: 1.35;

  ${props => props.styles};
`;
