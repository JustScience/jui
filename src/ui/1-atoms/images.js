import React from 'react';
import styled from 'styled-components';

import layout from './../0-particles/layout';

import imagePlaceholder from './../0-particles/images/img-placeholder.png';

const Frame = styled.div`
	margin: 0;
	padding: 0;
	max-width: ${props =>
		props.width ? props.width :
		props.maxWidth ? props.maxWidth
		: '100%'};

	${props => props.styles};
`;

const Picture = styled.img.attrs({
	src: props => props.src,
})`
	max-width: 100%;
	border-radius: ${layout.radius.default};

	${props => props.styles};
`;

const IMG = (props) => {
	let src = props.src || imagePlaceholder;
	return (
		<Frame>
			<Picture src={src} />
		</Frame>
	)
};

export default IMG;
