import styled from 'styled-components';

import color from './../0-particles/color';

const Section = styled.section`
	background: ${props => 
		props.Primary ? color.primary.gradient : 
		props.Secondary ? color.secondary.gradient : 
		props.Accent ? color.accent : 
		props.White ? color.White : 
		props.offWhite ? color.offWhite : 
		props.Black ? color.black : 
		color.white
	};
	margin: 0;
	padding: ${props => props.Pad ? '24px' : '0'};
	width: 100%;

	&:before,
	&:after {
		content: " ";
		display: table;
	}
	&:after {
		clear: both;
	}

	${props => props.styles};
`;

export default Section;