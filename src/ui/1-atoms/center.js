import styled from 'styled-components';

const Center = styled.div`
	margin-left: auto;
	margin-right: auto;
	text-align: center;
	align-content: center;
	justify-content: space-around;

	${props => props.styles};
`;

export default Center;