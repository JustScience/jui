import styled from 'styled-components';

export const Contain = styled.div`
	margin: 0 auto;
	padding: ${props => props.Pad ? '24px' : '0'};
	width: 94%;
	max-width: ${props => props.Narrow ? '540px' : '1080px'};

	${props => props.styles};
`;

export default Contain;