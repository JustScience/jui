import styled, { css } from 'styled-components';

const listBase = css`
	margin: 0;
	padding: 0;
	list-style: none;
`;

const UL = styled.ul`
	${listBase}
	li {
		display: ${props => props.Inline? 'inline-block' : 'block'};
	}

	${props => props.styles};
`;

export default UL;
