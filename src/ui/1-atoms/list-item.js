import React from 'react';
import styled, { css } from 'styled-components';

const listItemBase = css`
	margin: 0;
	padding: 12px 18px;
	text-decoratiron: none;
`;

const ListItem = styled.li`
	${listItemBase}
	
	${props => props.styles};
`;

const LI = (props) => {
	return (
		<ListItem>{props.children}</ListItem>
	)
};

export default LI;
