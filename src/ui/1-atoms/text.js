import styled from 'styled-components';

import color from './../0-particles/color';
import font from './../0-particles/font';

const TextBase = styled.p`
	@import url('https://fonts.googleapis.com/css?family=Nunito+Sans:300,500,700,900');
	color: ${props => 
		props.Light ? font.light : 
		props.Dark ? font.dark : 
		props.Primary ? color.primary.base : 
		props.Secondary ? color.secondary.base : 
		font.color
	};
	opacity: ${props => 
		props.Fadeder ? '0.3' : 
		props.Faded ? '0.6' : 
		'1'
	};
  	font-family: 'Nunito Sans', helvetica, sans-serif;
	font-size: ${props =>
		props.Tiny ? font.tiny :
		props.Smaller ? font.smaller :
		props.Small ? font.small :
		props.Big ? font.big :
		props.Bigger ? font.bigger :
		props.Giant ? font.giant :
		props.Huge ? font.huge :
		font.base
	}
	font-weight: ${props =>
		props.Thin ? font.thin :
		props.Thick ? font.thick :
		props.Thicker ? font.thicker :
		font.weight
	}
	line-height: ${props =>
		props.Squeeze ? '1.2' :
		props.Breathe ? '1.5' :
		props.Spread ? '1.8' :
		'1.35'
	}
	margin: 24px 0;
	text-align: ${props => 
		props.Left ? 'left' : 
		props.Center ? 'center' : 
		props.Right ? 'right' : 
		props.Justify ? 'justify' : 
		'left'
	};
`;

export const P = TextBase.withComponent('p').extend`


	${props => props.styles};
`;

export const Small = TextBase.withComponent('small').extend`
	font-size: 75%;

	${props => props.styles};
`

export const B = TextBase.withComponent('b').extend`
	font-weight: 700;

	${props => props.styles};
`;

export const Strong = TextBase.withComponent('strong').extend`
	font-weight: 900;

	${props => props.styles};
`

export const I = TextBase.withComponent('i').extend`
	font-style: italic;

	${props => props.styles};
`

export const EM = TextBase.withComponent('em').extend`
	font-style: italic;
	font-weight: 700;

	${props => props.styles};
`

export const U = TextBase.withComponent('u').extend`


	${props => props.styles};
`

export const ABBR = TextBase.withComponent('abbr').extend`
	font-style: italic;
	font-weight: 700;

	${props => props.styles};
`

export const BlockQuote = TextBase.withComponent('blockquote').extend`
	font-style: italic;
	font-weight: 700;

	${props => props.styles};
`

export const Pre = TextBase.withComponent('pre').extend`


	${props => props.styles};
`

export const Code = TextBase.withComponent('code').extend`


	${props => props.styles};
`

export const KBD = TextBase.withComponent('kbd').extend`


	${props => props.styles};
`

export const Samp = TextBase.withComponent('samp').extend`


	${props => props.styles};
`

export const Cite = TextBase.withComponent('cite').extend`
	font-style: italic;
	font-weight: 700;

	${props => props.styles};
`

export const Del = TextBase.withComponent('del').extend`
	font-style: italic;
	font-weight: 300;
	text-decoration: line-through

	${props => props.styles};
`

export const Mark = TextBase.withComponent('mark').extend`
	background: ${color.secondary.base};
	color: ${color.white}

	${props => props.styles};
`

export const Q = TextBase.withComponent('q').extend`
	font-style: italic;
	font-weight: 400;

	${props => props.styles};
`

export const Sub = TextBase.withComponent('sub').extend`


	${props => props.styles};
`

export const Sup = TextBase.withComponent('sup').extend`


	${props => props.styles};
`

export const Time = TextBase.withComponent('time').extend`


	${props => props.styles};
`
