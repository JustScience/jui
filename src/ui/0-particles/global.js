// LIBRARIES
import { injectGlobal } from 'styled-components';

// VARIABLES
import color from './color';
import fx from './fx';
import layout from './layout';

require('./normalize.css');

// STYLES
export default (props) => injectGlobal`
	@import url('https://fonts.googleapis.com/css?family=Montserrat:300,500,700,800,900');

	* {
		box-sizing: border-box;
		position: relative;
		box-shadow: ${props =>
			props.BoxShadow ? fx.shadow.box :
			'none'
		};
		text-shadow: ${props =>
			props.TextShadow ? fx.shadow.text :
			'none'
		};
	}

	body {
		font-family: 'Montserrat', helvetica, arial, sans-serif;
		-webkit-font-smoothing: antialiased;
		margin: 0;
		max-width: 100%;
		overflow-x: hidden;
	}

	a {
		text-decoration: none;
	}

	.clearfix {
		&:before,
		&:after {
			content: " ";
			display: table;
		}
		&:after {
			clear: both;
		}
	}


///////////////
// BASE LINK STYLES
///////////////

// a {
//     color: ${color.link};
//     text-decoration: none;

//     &,
//     &:active,
//     &.active {
//       &:focus,
//       &.focus {
//       }
//     }

//     &:hover {
//       color: ${color.linkHover};
//       text-decoration: none;
//     }
//     &:focus,
//     &.focus {
//       color: ${color.linkFocus};
//       text-decoration: none;
//     }

//     &:active,
//     &.active {
//       background-image: none;
//       outline: 0;
//     }

//     &.disabled,
//     &[disabled],
//     fieldset[disabled] & {
//       pointer-events: none; // Future-proof disabling of clicks on \`<a>\` elements
//     }

// }

////////////////////////
//// BASE FORM STYLES //
////////////////////////

input[type="checkbox"],
input[type="radio"] {
  cursor: pointer;
  display: inline-block;
  line-height: normal;
  margin-top: 1px;
  margin-right: calc(${layout.marginBase}*3);
  margin-bottom: calc(${layout.marginBase}*1);
  vertical-align: middle;
  width: auto;
}

input[type="search"] {
  box-sizing: border-box;
  -webkit-appearance: none;
}

input[type="file"] {
  display: block;
}

input[type="range"] {
  display: block;
  width: 100%;
}

`;

