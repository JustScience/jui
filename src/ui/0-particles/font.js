const font = {
	color: '#333333',
	dark: '#333333',
	light: '#eeeeee',
	faded: '#999999',
	fadeder: '#dddddd',
	base: '1.05em',
	big: '1.2em',
	bigger: '1.5em',
	huge: '1.8em',
	giant: '2.1em',
	small: '0.9em',
	smaller: '0.75em',
	tiny: '0.6em',
	weight: '500',
	thin: '300',
	thick: '700',
	thicker: '900',
}

export default font;