import color from './color';
import font from './font';

const form = {
	input: {
		BG: 'rgba(255,255,255,0.09)',
		focusBG: color.white,

		color: font.color,
		label: 'rgba(0,0,0,0.6)',
		placeholder: 'rgba(0,0,0,0.3)',
		placeholderDark: 'rgba(255,255,255,0.3)',
		placeholderPrimary: 'rgba(77, 62, 129, 0.3)',

		border: '1px solid rgba(255,255,255,0.0)',
		borderBottom: '1px solid ' + color.primary.base,
		focusBorderBottom: '1px solid ' + color.primary.base,
		focusBorderColor: color.mdGrey,

		shadow: 'inset 0 0 0 rgba(0,0,0,0.0)',
		focusShadow: 'inset 0 1px 3px ' + color.shadowLt,	
	},
}

export default form;