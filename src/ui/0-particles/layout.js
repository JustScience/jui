import theme from './_THEME';

const radius = {
	button: '6px',
	default: '30px',
	input: '6px',
	card: '9px',
}

const size = {
	headerHeight: theme.layout.size.headerHeight,
}

const space = {
	margin: '3px',
	padding: '3px',
}

const zindex = {
	breadcrumbs: 7500,
	dropdown: 1000,
	header: 8000,
	headerFixed: 8030,
	modal: 1050,
	modalBackground: 1040,
	mobileNav: 7000,
	mobileNavBurger: 9900,
	popover: 1060,
	tooltip: 1070,
}

const layout = {
	radius,
	size,
	space,
	zindex,
}

export default layout;
