import theme from './_THEME';

const site = {
	copyrightYear: theme.site.copyrightYear,
	copyrightOwner: theme.site.copyrightOwner,
}

export default site;