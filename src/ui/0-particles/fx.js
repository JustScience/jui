import color from './color';

const shadow = {
	box: '0 9px 15px -12px ' + color.shadowMd,
	boxLt: '0 9px 15px -12px ' + color.shadowLt,
	boxMd: '0 9px 15px -12px ' + color.shadowMd,
	boxDk: '0 9px 15px -12px ' + color.shadowDk,
	text: '0 1px 3px ' + color.shadowMd,
	textLt: '0 1px 1px ' + color.shadowLt,
	textMd: '0 1px 2px ' + color.shadowLt,
	textDk: '0 1px 2px ' + color.shadowMd,
};

const transition = 'all 333ms ease-in-out';

const fx = {
	shadow,
	transition,
};

export default fx;