// LIBRARIES
import React from 'react';
import { Link } from "react-router-dom";
import styled from 'styled-components';

// ASSETS
import logoIMG from './../../logo.svg';

// STYLES
import color from "./../0-particles/color";
import fx from "./../0-particles/fx";
import layout from "./../0-particles/layout";

// COMPONENTS
import Contain from "./../../ui/1-atoms/contain"
import UL from "./../../ui/1-atoms/list"
import LI from "./../../ui/1-atoms/list-item"
import FlexRow from "./../../ui/2-molecules/flex-row"
import MobileNav from "./../../ui/3-organisms/mobile-nav"

// STYLES
const HeaderWrap = styled.header`
  height: ${layout.size.headerHeight};
  margin: 0;
  padding: 0;
  width: 100%;
  background: ${color.header.bg};
`;

const Logo = styled.div`
  height: 100%;
  padding: 9px;
`;

const LogoIMG = styled.img.attrs({
  src: props => props.src,
})`
  height: 39px;
  object-fit: contain;
`;

const Nav = styled.nav`
  width: 100%;

  ul {
    align-items: center;
    display: flex;
    flex-direction: row;
    height: 100%;
    justify-content: flex-end;
    list-style: none;
    margin: 0;
    padding: 0;
    width: 100%;

    li {
      background: ${color.header.link.bg};
      color: ${color.header.link.text};
      height: 100%;
      padding: 21px;
      transition: ${fx.transition};

      &:hover {
        background: ${color.header.link.hover.bg};
        color: ${color.header.link.hover.text};
      }
    }
  }
`;

// TEMPLATE
const MainNav = (props) => {
  return (
    <Nav className={'top-nav'}>
      <UL Inline className={'top-nav_list'} >
        <Link to="/type"><LI className={'top-nav_link'}>Type</LI></Link>
        <Link to="/buttons"><LI className={'top-nav_link'}>Buttons</LI></Link>
        <Link to="/icons"><LI className={'top-nav_link'}>Icons</LI></Link>
        <Link to="/lists"><LI className={'top-nav_link'}>Lists</LI></Link>
        <Link to="/tables"><LI className={'top-nav_link'}>Tables</LI></Link>
        <Link to="/forms"><LI className={'top-nav_link'}>Forms</LI></Link>
      </UL>
    </Nav>
  )
};

// RENDER
const Header = (props) => {
  let logo = props.src || logoIMG;

  return (
    <React.Fragment>
      <HeaderWrap className={'header'}>
        <Contain>
          <FlexRow Inline noWrap Between>
            <Link to="/">
              <Logo className={'header_logo'}>
                <LogoIMG src={logo} alt='brand logo' />
              </Logo>
            </Link>
            <MainNav />
            <MobileNav />
          </FlexRow>
        </Contain>
      </HeaderWrap>
    </React.Fragment>
  )
};

// EXPORT
export default Header;