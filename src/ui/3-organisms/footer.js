// LIBRARIES
import React from 'react';
import styled, { css } from 'styled-components';

// STYLES
import color from "./../0-particles/color";
import site from "./../0-particles/site";

// COMPONENTS
import Contain from "./../../ui/1-atoms/contain"
import { P } from "./../../ui/1-atoms/text"

// CSS
const baseStyles = css`
  height: auto;
  margin: 0;
  padding: 0;
  width: 100%;
`;

const FootWrap = styled.footer`
  ${baseStyles}
  background: ${color.footer};

  &:before,
  &:after {
    content: " ";
    display: table;
  }
  &:after {
    clear: both;
  }
`;

const CopyrightWrap = styled.div`
  padding: 45px auto;
  p {
    color: ${color.footerText}
  }
`;

const Copyright = (props) => {
  return (
    <CopyrightWrap>
      <P Center Small>
        &copy; {props.year} {props.owner}. All rights reserved.
      </P>
    </CopyrightWrap>
  )
};

// RENDER
const Footer = (props) => {
  return (
    <FootWrap>
      <Contain>
        <Copyright year={site.copyrightYear} owner={site.copyrightOwner} />
      </Contain>
    </FootWrap>
  )
};

// EXPORT
export default Footer;