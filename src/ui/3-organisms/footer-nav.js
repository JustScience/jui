// import React from "react";
// import styled from "styled-components";

// // VARIABLES
// import color from "../../styles/color";
// import font from "../../styles/font";
// import layout from "../../styles/layout";
// import media from "../../styles/media";

// // COMPONENTS
// import Contain from "../contain/contain";
// import LogoLight from "../logo/logo-light";
// import SocialLinks from "../social/social";

// const FooterWrap = styled.footer`
//   width: 100%;
//   margin: 0;
//   padding: calc(${layout.paddingBase}*10);
//   background: ${color.footer};
// `;

// const Column = styled.div`
//   width: 100%;
//   display: inline-flex;
//   margin-bottom: 30px;

//   &:first-of-type {
//     width: 100%;

//     div {
//       display: inline-flex;
//       margin-left: auto;
//       margin-right: auto;
//     }
//   }

//   ${media.phone`width:50%;`};
//   ${media.tabletSM`width:50%;`};
//   ${media.tablet`width:25%;`};
//   ${media.tabletLG`
//     width:20%;
//     &:first-of-type {
//       float: left;
//       width: 20%;
//       div {
//         display: flex;
//         flex-direction: column;
//       }
//     }
//   `};
// `;

// const FootNavList = styled.ul`
//   color: ${color.white};
//   font-size: ${font.baseSize};
//   width: 100%;
//   margin-left: calc(${layout.marginBase}*10);
//   ${media.laptopSM`
//     margin-left: calc(${layout.marginBase}*20);
//   `};
// `;

// const ListHeader = styled.h6`
//   color: ${color.white};
//   font-size: calc(${font.baseSize}*1.2);
//   font-weight: ${font.thick};
//   margin: 0;
//   margin-bottom: calc(${layout.marginBase}*5);
//   padding: 0;
//   text-transform: uppercase;
// `;

// const A = styled.a.attrs({
//   href: (props) => props.href,
// })`
//   color: ${color.white};
//   font-size: ${font.baseSize};
//   cursor: pointer;
//   margin-bottom: calc(${layout.marginBase}*5);
//   width: auto;
//   outline: none;
//   border: none;
//   text-decoration: none;

//   &:hover {
//     color: ${color.white};
//   }
// `;

// const LI = styled.li`
//   width: auto;
// `;

// const FooterCopyright = styled.p`
//   color: ${color.white};
//   font-weight: ${font.baseWeight};
//   margin: calc(${layout.marginBase}*10) auto 0 auto;
//   text-align: center;
// `;

// const FootNavListHeader = (props) => {
//   return <ListHeader>{props.headerText}</ListHeader>;
// };

// const FootNavLink = (props) => {
//   return (
//     <A href={props.href}>
//       <LI>{props.linkText}</LI>
//     </A>
//   );
// };

// const Footer = (props) => {
//   return (
//     <FooterWrap>
//       <Contain>
//         <Column className="column">
//           <div>
//             <LogoLight maxWidth={"90%"} />
//             <SocialLinks />
//           </div>
//         </Column>
//         <Column className="column">
//           <FootNavList>
//             <FootNavListHeader headerText={"Header"} />
//             <FootNavLink linkText={"Link Text"} href={"http://www.jgalenti.com"} />
//             <FootNavLink linkText={"Link Text"} href={"http://www.jgalenti.com"} />
//             <FootNavLink linkText={"Link Text"} href={"http://www.jgalenti.com"} />
//           </FootNavList>
//         </Column>
//         <Column className="column">
//           <FootNavList>
//             <FootNavListHeader headerText={"Header"} />
//             <FootNavLink linkText={"Link Text?"} href={"http://www.jgalenti.com"} />
//             <FootNavLink linkText={"Link Text"} href={"http://www.jgalenti.com"} />
//           </FootNavList>
//         </Column>
//         <Column className="column">
//           <FootNavList>
//             <FootNavListHeader headerText={"Header"} />
//             <FootNavLink linkText={"Link Text"} href={"http://www.jgalenti.com"} />
//             <FootNavLink linkText={"Link Text"} href={"http://www.jgalenti.com"} />
//             <FootNavLink linkText={"Link Text"} href={"http://www.jgalenti.com"} />
//             <FootNavLink linkText={"Link Text"} href={"http://www.jgalenti.com"} />
//             <FootNavLink linkText={"Link Text"} href={"http://www.jgalenti.com"} />
//           </FootNavList>
//         </Column>
//         <Column className="column">
//           <FootNavList>
//             <FootNavListHeader headerText={"Header"} />
//             <FootNavLink linkText={"Link Text"} href={"http://www.jgalenti.com"} />
//             <FootNavLink linkText={"Link Text"} href={"http://www.jgalenti.com"} />
//             <FootNavLink linkText={"Link Text"} href={"http://www.jgalenti.com"} />
//             <FootNavLink linkText={"Link Text"} href={"http://www.jgalenti.com"} />
//             <FootNavLink linkText={"Link Text"} href={"http://www.jgalenti.com"} />
//           </FootNavList>
//         </Column>
//         <FooterCopyright>&copy; 2018 IN:DEEP Arts L.L.C. All rights reserved.</FooterCopyright>
//       </Contain>
//     </FooterWrap>
//   );
// };

// export default Footer;
