import React from 'react';
import styled, { css } from 'styled-components';

import font from "./../0-particles/font";
import layout from "./../0-particles/layout";

import imagePlaceholder from './../0-particles/images/img-placeholder.png';

const cardRadius = layout.radius.card;

const cardBase = css`
	display: flex;
	flex-direction: column;
	margin: 0;
	padding: 0;
`;


const CardWrap = styled.div`
	${cardBase}
	align-items: stretch;
	display: flex;
	flex-direction: column;
	width: ${props =>
		props.width ? props.width 
		: '100%'};
`;

const CardBG = styled.div`
	${cardBase}
	background: white;
	border-radius: ${cardRadius};
	// border: 1px solid rgba(0,0,0,0.03); // For added contrast against white background
	box-shadow: 0 9px 15px -12px rgba(0,0,0,0.9);
	height: 100%;
`;

const CardHead = styled.div`
	${cardBase}
	border-radius: ${cardRadius} ${cardRadius} 0 0;
	height: ${props => props.imgHeight ? props.imgHeight : '0'};
	max-height: 270px;
	overflow: hidden;
	padding: 0;
	width: 100%;
`;

const CardIMG = styled.img.attrs({
	height: props => props.imgHeight,
	src: props => props.src,
})`
	${cardBase}
	height: 100%;
	object-fit: cover;
	width: 100%;
`;

const CardContent = styled.div`
	${cardBase}
	padding: 18px 24px;
	width: 100%;
`;

const CardTitle = styled.h4`
	${cardBase}
	font-size: ${font.big};
	font-weight: ${font.thick};
	margin: 0;
	margin-bottom: 9px;
	padding: 0;
	width: 100%;
`;

const Card = (props) => {
	let imgHeight = props.imgHeight;
	let title = props.title;
	let src = props.src || imagePlaceholder;
 
	return (
		<CardWrap width={props.width}>
			<CardBG>
				<CardHead imgHeight={imgHeight}>
					<CardIMG src={src} />
				</CardHead>
				<CardContent>
					<CardTitle>{title}</CardTitle>
					{props.children}
				</CardContent>
			</CardBG>
		</CardWrap>
	)
};

export default Card;