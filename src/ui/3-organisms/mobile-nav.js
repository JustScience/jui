import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";

import color from "./../0-particles/color";
import font from "./../0-particles/font";
import layout from "./../0-particles/layout";
import media from "./../0-particles/media";

const BurgerWrap = styled.div`
  padding: 9px;
  width: 45px;
  float: right;
  margin-right: 9px;
  display: block;
  cursor: pointer;
  transform: scale(1);
  transition: all 333ms ease-out;

  &:hover {
    transform: scale(1.05);
    svg {
      fill: ${color.secondary.base};
    }
  }

  ${media.tabletLG`display: none;`} &:before,
  &:after {
    content: " ";
    clear: both;
    display: block;
    font-size: 0;
    height: 0;
    visibility: hidden;
  }
`;

const BurgerSVG = styled.svg`
  cursor: pointer;
  fill: ${color.primary.base};
  height: 39px;
  margin: 0;
  transition: all 333ms ease-in-out;
  width: 39px;

  &:hover {
    fill: ${color.secondary.base};
  }
`;

const Burger = (props) => {
  return (
    <BurgerSVG className={'nav-burger'} style={{enableBackground:'new 0 0 32 32'}} version="1.1" viewBox="0 0 32 32" width="35px">
      <g>
        <path id="bg-top" d="M4,10h24c1.104,0,2-0.896,2-2s-0.896-2-2-2H4C2.896,6,2,6.896,2,8S2.896,10,4,10z" />
        <path id="bg-mid" d="M28,14H4c-1.104,0-2,0.896-2,2  s0.896,2,2,2h24c1.104,0,2-0.896,2-2S29.104,14,28,14z" />
        <path id="bg-bot" d="M28,22H4c-1.104,0-2,0.896-2,2s0.896,2,2,2h24c1.104,0,2-0.896,2-2  S29.104,22,28,22z" />
      </g>
    </BurgerSVG>
  )
};

const MobileNavWrap = styled.div`
  background: ${color.primary.gradient};
  margin: 0;
  padding: 0 calc(${layout.space.padding}*5);
  position: fixed;
  top: ${props => 
    props.top ? props.top 
    : "60px"};
  left: 0;
  right: 0;
  bottom: 0;
  overflow: hidden;
  height: 0;
  transition: all 333ms ease-in-out;
  z-index: 9990;

  &.--is-open {
    height: 100%;
    padding: calc(${layout.space.padding}*5);
  }

  ${media.tabletLG`
    display: none;
  `} 

  &:before,
  &:after {
    content: " ";
    clear: both;
    display: block;
    font-size: 0;
    height: 0;
    visibility: hidden;
  }
`;

const MobileNavList = styled.ul`
  padding-left: 0;
  list-style: none;
  margin-top: 60px;
  margin-left: auto;
  margin-right: auto;
  width: auto;
  max-width: 300px;
`;

const MobileNavListItem = styled.ul`
  color: ${color.white};
  text-transform: uppercase;
  display: block;
  padding: 24px 12px;
  cursor: pointer;
  font-size: 110%;
  margin: 0 auto;
  text-align: center;
  font-weight: ${font.thick};
  transition: all 333ms ease-in-out;
  transform: scale(1);
  width: auto;

  &:hover {
    color: ${color.white};
    text-shadow: 0 1px 6px ${color.shadowLt};
    transform: scale(1.05);
  }
`;

function MobileNavToggle() {
  let nav = document.getElementById("mobile-nav");

  if (nav.getAttribute("class").includes("--is-open")) {
    nav.classList.remove("--is-open");
  } else {
    nav.classList.add("--is-open");
  }
}

const NavBurger = (props) => {
  return (
    <BurgerWrap onClick={MobileNavToggle}>
      <Burger className={"nav-burger"} />
    </BurgerWrap>
  );
};

const MobileNav = (props) => {
  return (
    <div>
      <NavBurger />
      <MobileNavWrap className="mobile-nav_wrap" id={"mobile-nav"} top={props.top}>
        <MobileNavList className="mobile-nav_list">
          <Link to="/" onClick={MobileNavToggle} >
            <MobileNavListItem className="mobile-nav_list-item trigger">Home</MobileNavListItem>
          </Link>
          <Link to="/type" onClick={MobileNavToggle} >
            <MobileNavListItem className="mobile-nav_list-item trigger">Type</MobileNavListItem>
          </Link>
          <Link to="/buttons" onClick={MobileNavToggle} >
            <MobileNavListItem className="mobile-nav_list-item trigger">Buttons</MobileNavListItem>
          </Link>
          <Link to="/icons" onClick={MobileNavToggle} >
            <MobileNavListItem className="mobile-nav_list-item trigger">Icons</MobileNavListItem>
          </Link>
          <Link to="/lists" onClick={MobileNavToggle} >
            <MobileNavListItem className="mobile-nav_list-item trigger">Lists</MobileNavListItem>
          </Link>
          <Link to="/tables" onClick={MobileNavToggle} >
            <MobileNavListItem className="mobile-nav_list-item trigger">Tables</MobileNavListItem>
          </Link>
          <Link to="/forms" onClick={MobileNavToggle} >
            <MobileNavListItem className="mobile-nav_list-item trigger">Forms</MobileNavListItem>
          </Link>
          {props.children}
        </MobileNavList>
      </MobileNavWrap>
    </div>
  );
};

export default MobileNav;
