import React from 'react';
import styled from 'styled-components';

// ASSETS
// import burger from './../1-atoms/icons/burger.svg';

// STYLES
import color from "./../0-particles/color";


// COMPONENTS
const BurgerSVG = styled.svg`
	cursor: pointer;
	fill: ${color.primary.base};
	height: 39px;
	margin: 0;
	transition: all 333ms ease-in-out;
	width: 39px;

	&:hover {
		fill: ${color.secondary.base};
	}

	${props => props.styles};
`;

// const BurgerIMG = styled.img.attrs({
// 	src: props => props.src,
// })`
// 	background: pink;
// 	float: right;
// 	fill: ${color.primary};
// 	height: 45px;
// 	width: 45px;
// 	padding: 6px 7px 5px 7px;
// 	transition: all 333ms ease-in-out;

// 	&:hover {
// 		fill: ${color.secondary};
// 	}
// `;

const NavBurger = (props) => {
	return (
		<BurgerSVG className={'nav-burger'} style={{enableBackground:'new 0 0 32 32'}} version="1.1" viewBox="0 0 32 32" width="35px">
			<g>
				<path id="bg-top" d="M4,10h24c1.104,0,2-0.896,2-2s-0.896-2-2-2H4C2.896,6,2,6.896,2,8S2.896,10,4,10z" />
				<path id="bg-mid" d="M28,14H4c-1.104,0-2,0.896-2,2  s0.896,2,2,2h24c1.104,0,2-0.896,2-2S29.104,14,28,14z" />
				<path id="bg-bot" d="M28,22H4c-1.104,0-2,0.896-2,2s0.896,2,2,2h24c1.104,0,2-0.896,2-2  S29.104,22,28,22z" />
			</g>
		</BurgerSVG>
	)
};

export default NavBurger;
