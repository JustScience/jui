import styled from 'styled-components';

import color from './../0-particles/color';
import font from './../0-particles/font';

export const Meter = styled.meter`
	background: ${color.ltGrey}

	${props => props.styles};
`

export const Progress = styled.progress`
	background: ${color.mdGrey}

	${props => props.styles};
`

