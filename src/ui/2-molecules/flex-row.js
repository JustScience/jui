import styled from 'styled-components';

const FlexRow = styled.div`
	display: ${props => 
		props.Inline ? 'inline-flex'
		: 'flex'};
	flex-direction: row;
	flex-wrap: ${props => 
		props.noWrap ? 'no-wrap' : 
		props.revWrap ? 'reverse-wrap' 
		: 'wrap'};
	height: 100%;
	justify-content: ${props =>
		props.Between ? 'space-between' :
		props.Start ? 'flex-start' :
		props.End ? 'flex-end' :
		props.Center ? 'center' :
		props.Even ? 'space-evenly'
		: 'space-around'};
	}
	width: 100%;

	${props => props.styles};
`;

export default FlexRow;
