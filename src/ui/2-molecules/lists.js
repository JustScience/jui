import styled, { css } from 'styled-components';

import color from "./../0-particles/color";
import font from "./../0-particles/font";

const listBase = css`
  list-style: none;
  margin: 0;
  padding: 0;

  li {
    color: ${props =>
      props.Light ? font.light : 
      props.Dark ? font.dark : 
      props.Primary ? color.primary.base : 
      props.Secondary ? color.secondary.base : 
      font.color };
    display: ${props => props.Inline? 'inline-block' : 'block'};
  }
`;

const itemBase = css`
  padding-bottom: 6px;
  padding-right: 12px;
`;


export const OL = styled.ol`
  ${listBase}

  ${props => props.styles};
`;

export const UL = styled.ul`
  ${listBase}

  ${props => props.styles};
`;

export const LI = styled.li`
  ${itemBase}

  ${props => props.styles};
`;

export const GL = styled.ul`
  ${listBase}
  align-content: stretch;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-evenly;
  padding-left: 12px;

  ${props => props.styles};
`;

export const GI = styled.li`
  ${itemBase}
  display: flex;
  margin-bottom: 3px;

  ${props => props.styles};
`;
