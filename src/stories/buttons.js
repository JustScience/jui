import React from 'react';

import { setAddon, storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';
import { withKnobs, text, boolean, number, select } from '@storybook/addon-knobs/react';

import Section from './../ui/1-atoms/section';
import Contain from './../ui/1-atoms/contain';
import { H3, H5 } from './../ui/1-atoms/headings'
import { Spacer } from './../ui/1-atoms/layout'
import BTN from './../ui/1-atoms/buttons';


// GLOBAL CSS
import baseStyles from './../ui/0-particles/global';
baseStyles();

import JSXAddon from 'storybook-addon-jsx';
setAddon(JSXAddon);

action('button-click');


const buttons = storiesOf('Buttons', module);
buttons.addDecorator(withKnobs);

buttons.addWithJSX('Flat Buttons', () => (
	<React.Fragment>
		<Section Accent>
			<Contain Pad>

				<H3>Flat Buttons</H3>
				<Spacer />

				<BTN 
					Inline Space 
					label='Default' 
				/>

				<BTN 
					Light Inline Space
					label='Light' 
				/>

				<BTN 
					Dark Inline Space 
					label='Dark' 
				/>

				<BTN 
					Primary Inline Space
					label='Primary' 
				/>

				<BTN 
					Secondary Inline Space
					label='Secondary' 
				/>

			</Contain>
		</Section>
	</React.Fragment>
));

buttons.addWithJSX('Raised Buttons', () => (
	<React.Fragment>
		<Section Accent>
			<Contain Pad>

				<H3>Raised Buttons</H3>
				<Spacer />

				<BTN 
					Raised Inline Space 
					label='Default' 
				/>

				<BTN 
					Raised Light Inline Space
					label='Light' 
				/>

				<BTN 
					Raised Dark Inline Space 
					label='Dark' 
				/>

				<BTN 
					Raised Primary Inline Space
					label='Primary' 
				/>

				<BTN 
					Raised Secondary Inline Space
					label='Secondary' 
				/>

			</Contain>
		</Section>
	</React.Fragment>
));
