import React from 'react';

import { setAddon, storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';
import { withKnobs, text, boolean, number, select } from '@storybook/addon-knobs/react';
import { setOptions } from '@storybook/addon-options';

import Contain from './../ui/1-atoms/contain';
import Form from './../ui/1-atoms/inputs/form';
import Input from './../ui/1-atoms/inputs/input';
import Section from './../ui/1-atoms/section';

// GLOBAL CSS
import baseStyles from './../ui/0-particles/global';
baseStyles();

import JSXAddon from 'storybook-addon-jsx';
setAddon(JSXAddon);

const forms = storiesOf('Forms', module);
forms.addDecorator(withKnobs);

forms.addWithJSX('Input Field', () => (
	<div>
		<Section Secondary Pad>
			<Contain>
				<Form title={text('Form Title', 'Dark (default)')} >
					<Input 
						type={select('Type', ['text','number','password','email'], 'text')} 
						htmlFor={text('htmlFor', 'demo')} 
						labelText={text('Label', 'Label Text')} 
						placeholder={text('Placeholder', 'Placeholder')} 
					/>
				</Form>
			</Contain>
		</Section>	
		<Section Pad>
			<Contain>
				<Form Primary title={text('Form Title', 'Primary')} >
					<Input 
						type={select('Type', ['text','number','password','email'], 'text')} 
						htmlFor={text('htmlFor', 'demoPrimary')} 
						labelText={text('Label', 'Label Text')} 
						placeholder={text('Placeholder', 'Placeholder')} 
					/>
				</Form>
			</Contain>
		</Section>
		<Section Primary Pad>
			<Contain>
				<Form Dark title={text('Form Title', 'Dark')} >
					<Input 
						type={select('Type', ['text','number','password','email'], 'text')} 
						htmlFor={text('htmlFor', 'demoLight')} 
						labelText={text('Label', 'Label Text')} 
						placeholder={text('Placeholder', 'Placeholder')} 
					/>
				</Form>
			</Contain>
		</Section>
	</div>
  ));
