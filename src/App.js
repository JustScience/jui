import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";

// import logo from './logo.svg';

// COMPONENTS
import Header from "./ui/3-organisms/header"
import Footer from "./ui/3-organisms/footer"
import ResponsiveGuide from "./ui/0-particles/responsive-guide"

// PAGES
import Home from "./pages/home"
import Type from "./pages/type"
import Icons from "./pages/icons"
import Buttons from "./pages/buttons"
import Lists from "./pages/lists"
import Tables from "./pages/tables"
import Forms from "./pages/forms"

// GLOBAL CSS
import baseStyles from './ui/0-particles/global';
baseStyles();

const App = () => (
  <Router>
	<div>
		<Header />

		<Route exact path="/" component={Home} />
		<Route exact path="/type" component={Type} />
		<Route exact path="/icons" component={Icons} />
		<Route exact path="/buttons" component={Buttons} />
		<Route exact path="/lists" component={Lists} />
		<Route exact path="/tables" component={Tables} />
		<Route exact path="/forms" component={Forms} />

		<Footer />
		<ResponsiveGuide/>
	</div>
  </Router>
);

export default App;

