import { configure } from '@storybook/react';
import { configureViewport } from '@storybook/addon-viewport';
import '@storybook/addon-console';
import { addDecorator } from '@storybook/react';
import { withConsole } from '@storybook/addon-console';
import * as storybook from '@storybook/react';
import { setOptions } from '@storybook/addon-options';



function loadStories() {
  require('../src/stories/index.js');
  require('../src/stories/buttons.js');
}

configure(loadStories, module);



setOptions({
  name: 'JSci UI',
  url: 'jsci.io',
  goFullScreen: false,
  showStoriesPanel: true,
  showAddonPanel: true,
  showSearchBox: false,
  addonPanelInRight: false,
  sortStoriesByKind: false,
  hierarchySeparator: null,
  hierarchyRootSeparator: null,
  sidebarAnimations: true,
  selectedAddonPanel: undefined, // The order of addons in the "Addon panel" is the same as you import them in 'addons.js'. The first panel will be opened by default as you run Storybook
  enableShortcuts: true, // true by default
});

// storybook.configure(() => require('./stories'), module);
addDecorator((storyFn, context) => withConsole()(storyFn)(context));

